import React from 'react';
import { StyleSheet } from 'react-native';
import Navigation from "./navigations";
import { DefaultTheme, Provider as PaperProvider } from "react-native-paper";
import theme from "./assets/styles/Themes";

export default function App() {

    return (
        /*<View style={styles.container}>
          <Navigation/>
        </View>*/
        <PaperProvider theme={theme}>
            <Navigation/>
        </PaperProvider>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
});
