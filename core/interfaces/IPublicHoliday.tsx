export interface IPublicHoliday {
    date: string;
    localName: string;
    name: string;
    countryCode: string;
    fixed: boolean;
    global: boolean;
    counties: number;
    launchYear: number;
    type: string;
}
