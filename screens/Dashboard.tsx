import * as React from 'react';
import HeaderMenu from '../components/HeaderMenu';
import { StyleSheet, View, Text } from 'react-native';
import { IPublicHoliday } from '../core/interfaces/IPublicHoliday';
import { Button } from 'react-native-paper';
import { TodayPublicHoliday } from '../components/TodayPublicHoliday';
import NextPublicHoliday from '../components/NextPublicHoliday';
import { CountryPicker } from '../components/CountryPicker';

interface IState {
    styles: any;
    route: any;
    date: {
        year: string;
        month: string;
        day: string;
    };
    dataSource: IPublicHoliday[];
    publicHolidayObj: {
        data: IPublicHoliday | undefined;
        isLoading: boolean;
    };
    nextPublicHolidaysObj: {
        data: IPublicHoliday[];
        isLoading: boolean;
        length: number;
    };
    langSelected: string;
}

export class Dashboard extends React.Component<any, IState> {
    constructor(props: any) {
        super(props);
        this.state = {
            styles: StyleSheet.create({
                container: {
                    flex: 1,
                    backgroundColor: '#fff',
                },
            }),
            route: {
                name: '',
            },
            date: {
                year: new Date().toISOString().slice(0, 10).split('-')[0],
                month: new Date().toISOString().slice(0, 10).split('-')[1],
                day: new Date().toISOString().slice(0, 10).split('-')[2],
            },
            dataSource: [],
            publicHolidayObj: {
                data: undefined,
                isLoading: false,
            },
            nextPublicHolidaysObj: {
                data: [],
                isLoading: false,
                length: 3,
            },
            langSelected: 'FR',
        };
    }

    render() {
        let more;
        if (this.state.nextPublicHolidaysObj.isLoading && !this.state.nextPublicHolidaysObj.length) {
            more = <Text></Text>;
        } else {
            more = (
                <Button
                    onPress={() => {
                        this.loadMorePublicHolidaysData();
                    }}
                >
                    <Text>More Data</Text>
                </Button>
            );
        }
        return (
            <View style={this.state.styles.container}>
                <HeaderMenu title={this.state.route.name} lang={this.state.langSelected} />
                <CountryPicker onChange={(value: any) => this.handleCountryLanguageChange(value)} />
                <TodayPublicHoliday
                    data={this.state.publicHolidayObj.data}
                    isLoading={this.state.publicHolidayObj.isLoading}
                />
                <NextPublicHoliday
                    data={this.state.nextPublicHolidaysObj.data}
                    isLoading={this.state.nextPublicHolidaysObj.isLoading}
                />
            </View>
        );
    }

    componentDidMount() {
        this.setState({
            publicHolidayObj: {
                data: this.state.publicHolidayObj.data,
                isLoading: true,
            },
            route: this.props.route,
        });
        this.setPublicHolidaysData(this.state.langSelected);
    }

    getPublicHolidayObjByDate(data: IPublicHoliday[]): { data: IPublicHoliday | undefined; isLoading: boolean } {
        data.forEach((el) => {
            if (el.date === `${this.state.date.year}-${this.state.date.month}-${this.state.date.day}`) {
                return { data: el, isLoading: false };
            }
        });
        return { data: undefined, isLoading: false };
    }

    getNextPublicHolidaysObj(
        data: IPublicHoliday[],
        length: number,
    ): { data: IPublicHoliday[]; isLoading: boolean; length: number } {
        let count = 0;
        const res: IPublicHoliday[] = [];
        while (res.length < length && data[count]) {
            const currentFormatDate = `${this.state.date.year}-${this.state.date.month}-${this.state.date.day}`;
            const currentTime = new Date(currentFormatDate).getTime();
            if (new Date(data[count].date).getTime() > currentTime && data[count].date != currentFormatDate) {
                res.push(data[count]);
            }
            count++;
        }

        return { data: res, isLoading: false, length: length };
    }

    loadMorePublicHolidaysData() {
        this.setState({
            nextPublicHolidaysObj: this.getNextPublicHolidaysObj(
                this.state.dataSource,
                this.state.nextPublicHolidaysObj.length + 3,
            ),
        });
    }

    setPublicHolidaysData(lang: string) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        headers.append('Origin', 'http://localhost:19006');
        const url = `https://date.nager.at/api/v2/publicholidays/${this.state.date.year}/${lang}`;
        fetch(url)
            .then((res) => res.json())
            .then((res) => {
                this.setState({
                    dataSource: res,
                    publicHolidayObj: this.getPublicHolidayObjByDate(res),
                    // publicHolidayObj: { data: res[0], isLoading: false },
                    nextPublicHolidaysObj: this.getNextPublicHolidaysObj(res, this.state.nextPublicHolidaysObj.length),
                });
                this.setState({});
                this.setState({});
            });
    }

    handleCountryLanguageChange(value: string) {
        this.setState({ langSelected: value });
        this.setPublicHolidaysData(value);
    }
}
