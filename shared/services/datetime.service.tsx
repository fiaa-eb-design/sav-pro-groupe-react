export function getAppLocaleDateString(date: string): string | undefined {
    if (date.split('-').length === 3) {
        return new Date(date).toLocaleDateString('en-GB');
    }

    return undefined;
}
