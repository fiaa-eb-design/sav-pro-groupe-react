import { View } from 'react-native';
import * as React from 'react';
import { IPublicHoliday } from '../core/interfaces/IPublicHoliday';
import { getAppLocaleDateString } from '../shared/services/datetime.service';
import { List } from 'react-native-paper';
import { Text, StyleSheet } from 'react-native';
import LoadingProgress from './LoadingProgress';

const styles = StyleSheet.create({
    title: {
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 15,
        marginBottom: 15,
        marginLeft: 10,
    },

    baseText: {
        fontWeight: 'bold',
    },
    innerText: {
        color: 'red',
    },
});

interface IProps {
    data: IPublicHoliday | undefined;
    isLoading: boolean;
}

interface IState {
    date: Date;
}

export class TodayPublicHoliday extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            date: new Date(),
        };
    }

    render() {
        let value;
        if (this.props.isLoading) {
            value = <Text> </Text>;
        } else {
            value = this.props.data ? (
                <List.Item
                    title={this.props.data.name}
                    description={getAppLocaleDateString(this.props.data.date)}
                    left={(props) => <List.Icon {...props} icon="calendar-text-outline" />}
                />
            ) : (
                <List.Item title="No Public Holiday" />
            );
        }

        return (
            <View>
                <Text style={styles.title}> Today </Text>
                {value}
                <LoadingProgress isLoading={this.props.isLoading} />
            </View>
        );
    }

    createData(name: string) {
        return { name };
    }
}
