import React from 'react';
import { View, Picker, StyleSheet } from 'react-native';

interface IProps {
    languages: { label: string; value: string }[];
    selected: { label: string; value: string };
    onChange: any;
}

export class CustomPicker extends React.Component<IProps, any> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            selected: this.props.selected,
        };
    }

    render() {
        const options: any[] = [];
        this.props.languages.map((value, index) => {
            options.push(<Picker.Item key={index} label={value.label} value={value.value} />);
        });
        return (
            <View style={styles.container}>
                <Picker
                    selectedValue={this.state.selected}
                    style={{ height: 50, width: 150 }}
                    onValueChange={(itemValue, itemIndex) => this.handleSelectOption(itemValue)}
                >
                    {options}
                </Picker>
            </View>
        );
    }

    handleSelectOption(value: { label: string; value: string }) {
        this.setState({ selected: value });
        this.props.onChange(value);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20,
        alignItems: 'flex-start',
        marginLeft: 10,
    },
});
