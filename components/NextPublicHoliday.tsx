import React from 'react';
import { IPublicHoliday } from '../core/interfaces/IPublicHoliday';
import { withStyles } from '@material-ui/core/styles';
import { StyleSheet, Text, View } from 'react-native';
import { getAppLocaleDateString } from '../shared/services/datetime.service';
import LoadingProgress from './LoadingProgress';
import { List } from 'react-native-paper';

interface IProps {
    data: IPublicHoliday[];
    isLoading: boolean;
}

const styles = StyleSheet.create({
    title: {
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 15,
        marginBottom: 15,
        marginLeft: 10,
    },

    baseText: {
        fontWeight: 'bold',
    },
    innerText: {
        color: 'red',
    },
});
class NextPublicHoliday extends React.Component<IProps, any> {
    constructor(props: IProps) {
        super(props);
    }

    render() {
        const array: any[] = [];
        this.props.data.map((value, index) => {
            array.push(
                <List.Item
                    key={index}
                    title={value.name}
                    description={getAppLocaleDateString(value.date)}
                    left={(props) => <List.Icon {...props} icon="calendar-text-outline" />}
                />,
            );
        });
        return (
            <View>
                <Text>
                    <Text style={styles.title}> Next Public Holidays </Text>
                </Text>
                {array}
                <LoadingProgress isLoading={this.props.isLoading} />
            </View>
        );
    }
}
export default withStyles(styles, { withTheme: true })(NextPublicHoliday);
