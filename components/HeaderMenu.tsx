import React from 'react';
import { Theme } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { StyleSheet, View } from 'react-native';

import { Appbar } from 'react-native-paper';

type Anchor = 'left';

const styles = (theme: Theme) => ({
    root: {
        marginBottom: theme.spacing(6),
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    country: {
        marginRight: 0,
    },
});

interface IProps {
    title: string;
    classes: any;
    lang: string;
}

interface IState {
    menuState: boolean;
    anchor: 'left';
    country: string;
}

class HeaderMenu extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            menuState: false,
            anchor: 'left',
            country: 'fr',
        };
    }

    render() {
        const styles = StyleSheet.create({
            country: {
                color: '#fff',
                textAlign: 'right',
            },
        });

        return (
            <View>
                <Appbar.Header>
                    <Appbar.Content title={this.props.title} />
                    <Appbar.Content titleStyle={styles.country} title={this.props.lang} />
                </Appbar.Header>
            </View>
        );
    }

    toogleMenuState(menuState: boolean): void {
        this.setState({ menuState: !menuState });
    }

    toogleSideBar = (open: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
        this.setState({ menuState: open });
    };
}

export default withStyles(styles, { withTheme: true })(HeaderMenu);
