import React from 'react';
import { CustomPicker } from './CustomPicker';
import { StyleSheet, Text, View } from 'react-native';

const styles = StyleSheet.create({
    root: {
        marginTop: 10,
        marginBottom: 40,
    },
    title: {
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 15,
        marginBottom: 15,
        marginLeft: 10,
    },

    baseText: {
        fontWeight: 'bold',
    },
    innerText: {
        color: 'red',
    },
});

interface IProps {
    onChange: any;
}

export class CountryPicker extends React.Component<IProps, any> {
    constructor(props: IProps) {
        super(props);
    }
    render() {
        const languages = [
            { label: 'Fr', value: 'FR' },
            { label: 'At', value: 'AT' },
        ];
        return (
            <View style={styles.root}>
                <Text>
                    <Text style={styles.title}> Change Language: </Text>
                </Text>
                <CustomPicker
                    languages={languages}
                    selected={languages[0]}
                    onChange={(value: string) => this.handleCountryChange(value)}
                ></CustomPicker>
            </View>
        );
    }

    handleCountryChange(newValue: string) {
        this.props.onChange(newValue);
    }
}
