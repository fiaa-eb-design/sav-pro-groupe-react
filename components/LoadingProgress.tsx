import React from 'react';
import { Theme, withStyles } from '@material-ui/core/styles';
import { ActivityIndicator, Colors } from 'react-native-paper';
import { View, Text } from 'react-native';

interface IProps {
    isLoading: boolean;
    classes: any;
}

const styles = (theme: Theme) => ({
    circularProgress: {
        marginLeft: theme.spacing(2),
    },
});

class LoadingProgress extends React.Component<IProps, any> {
    constructor(props: IProps) {
        super(props);
    }

    render() {
        const { classes } = this.props;
        let res;
        if (this.props.isLoading) {
            res = (
                <View>
                    <ActivityIndicator animating={true} color={Colors.blue900} />
                </View>
            );
        } else {
            res = (
                <View>
                    <Text></Text>
                </View>
            );
        }
        return res;
    }
}
export default withStyles(styles, { withTheme: true })(LoadingProgress);
